

#include "myEventHeader.h"


myEventHeader::myEventHeader()
: TObject()
, fTimeStamp(0.)
{
  // constructor
}

//_________________________________________________________________________
myEventHeader::myEventHeader(const myEventHeader &header)
: TObject(header)
, fTimeStamp(header.fTimeStamp)
{
  // copy constructor
}

//_________________________________________________________________________
myEventHeader::~myEventHeader()
{
  // destructor
}

//_________________________________________________________________________
void myEventHeader::Reset()
{
  //
  // Reset all values to default values
  //
  fTimeStamp=0.;
}
