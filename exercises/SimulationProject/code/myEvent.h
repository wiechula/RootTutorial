#ifndef myEvent_H
#define myEvent_H

#include <TObject.h>
#include <TClonesArray.h>

#include "myParticle.h"
#include "myEventHeader.h"

class myEvent : public TObject {
public:
  myEvent();
  myEvent(const myEvent &ev);
  virtual ~myEvent();

  void SetVz(Double_t vx) { fVz = vx; }
  Double_t GetVz() const { return fVz; }

  void Reset();
  
  Int_t GetNumberOfParticles() const { return fParticles.GetEntriesFast(); }

  // interface to internal particle array
  myParticle* AddParticle(const myParticle &particle);
  const myParticle& GetParticle(Int_t iparticle) const { return *static_cast<const myParticle*>(fParticles.At(iparticle)); }

  // interface to event header information
  void CreateHeader()                         { delete fHeader; fHeader=new myEventHeader; }
  const myEventHeader* GetEventHeader() const { return fHeader;                            }

  void   SetTimeStamp(UInt_t timeStamp)       { if (fHeader) CreateHeader(); fHeader->SetTimeStamp(timeStamp); }
  UInt_t GetTimeStamp() const                 { return fHeader?fHeader->GetTimeStamp():0.;                     }
  
private:
  Double_t fVz;                // x vertex of the event

  TClonesArray fParticles;     // array of particles

  myEventHeader *fHeader;      // event header information

  ClassDef(myEvent,1);         // event information
};


#endif
