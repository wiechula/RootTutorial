#ifndef myGenerator_H
#define myGenerator_H

#include <TObject.h>
#include <TString.h>

class TF1;
class TLorentzVector;

class myGenerator : public TObject {
public:
  myGenerator();
  virtual ~myGenerator();

  void   SetNumberOfEvents(UInt_t nev) { fNumberOfEvents=nev;    }
  UInt_t GetNumberOfEvents()   const   { return fNumberOfEvents; }

  void     SetParametrisationFileName(TString dist)  { fDistributions=dist;          }
  const char* GetParametrisationFileName() const     { return fDistributions.Data(); }

  void    SetOutputFileName(TString outfile)      { fOutputFile=outfile;          }
  const char* GetOutputFileName() const           { return fOutputFile;           }

  void Simulate(Int_t nevents=-1);
  
  void SetdEdxSignal(myParticle &part);
  // define as static to be able to use it
  // from everywhere
  static Double_t BetheBlochAleph(Double_t bg);

  void SmearPt(Double_t &pt);
  void AddDecayParticles(myEvent &ev, TF1* fpt);
  void DecayMother(const TLorentzVector &vMother,
                   TLorentzVector &vDaughter1,
                   TLorentzVector &vDaughter2,
                   Int_t decayType=4);
  
private:
  UInt_t  fNumberOfEvents;     // number of events to generate
  TString fDistributions;      // input file name with parametrisations for the distributions
  TString fOutputFile;         // output file name for the tree with the simulated data


  ClassDef(myGenerator,0);     // Class to make simple particle simulations
};
#endif

