
// ROOT includes
#include <TMath.h>
#include <TLorentzVector.h>

//package includes
#include "myParticle.h"

ClassImp(myParticle)

myParticle::myParticle()
: TObject()
, fPx(0.)
, fPy(0.)
, fPz(0.)
, fMass(0.)
, fVx(0.)
, fVy(0.)
, fVz(0.)
, fParticleType(0)
, fCharge(0.)
, fdEdxSignal(0.)
, fIsDetectable(kTRUE)
, fFirstDaughter(-1)
, fLastDaughter(-1)
, fMother(-1)
{
  // default constructor
}

//______________________________________________________________
myParticle::myParticle(const myParticle &ev)
: TObject(ev)
, fPx(ev.fPx)
, fPy(ev.fPy)
, fPz(ev.fPz)
, fMass(ev.fMass)
, fVx(ev.fVx)
, fVy(ev.fVy)
, fVz(ev.fVz)
, fParticleType(ev.fParticleType)
, fCharge(ev.fCharge)
, fdEdxSignal(ev.fdEdxSignal)
, fIsDetectable(ev.fIsDetectable)
, fFirstDaughter(ev.fFirstDaughter)
, fLastDaughter(ev.fLastDaughter)
, fMother(ev.fMother)
{
  // copy constructor
}

//______________________________________________________________
myParticle::~myParticle()
{
  // destructor
}

//______________________________________________________________
void myParticle::SetPtEtaPhiM(Double_t pt, Double_t eta, Double_t phi, Double_t m)
{
  // set momentum via pt, eta, phi
  pt = TMath::Abs(pt);
  SetPxPyPzM(pt*TMath::Cos(phi), pt*TMath::Sin(phi), pt*TMath::SinH(eta) ,m);
}

//______________________________________________________________
Double_t myParticle::GetP()  const
{
  // total momentum
  return TMath::Sqrt(fPx*fPx+fPy*fPy+fPz*fPz);
}

//______________________________________________________________
Double_t myParticle::GetPt()  const
{
  // transverse momentum
  return TMath::Sqrt(fPx*fPx+fPy*fPy);
}

//______________________________________________________________
Double_t myParticle::GetPhi() const
{
  // returns phi from -pi to pi
  return fPx == 0.0 && fPx == 0.0 ? 0.0 : TMath::ATan2(fPy,fPx);
}

//______________________________________________________________
Double_t myParticle::GetEta() const
{
  //return pseudo rapidity
  return -TMath::Log( TMath::Tan(GetTheta()/2.) );
}

//______________________________________________________________
Double_t myParticle::GetTheta() const
{
  //return the polar angle
  return fPx == 0.0 && fPy == 0.0 && fPz == 0.0 ? 0.0 : TMath::ATan2(GetPt(),fPz);
}

//______________________________________________________________
void myParticle::SetLorentzVector(TLorentzVector &v, Double_t massAssumption) const
{
  // construct a Lorentzvector from the particles properties
  if (massAssumption<0) massAssumption=fMass;
  v.SetXYZM(fPx, fPy, fPz, massAssumption);
}