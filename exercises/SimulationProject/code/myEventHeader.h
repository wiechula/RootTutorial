#ifndef myEventHeader_H
#define myEventHeader_H

#include <TObject.h>

class myEventHeader : public TObject {
public:
  myEventHeader();
  myEventHeader(const myEventHeader& header);
  ~myEventHeader();

  void   SetTimeStamp(UInt_t timeStamp) { fTimeStamp=timeStamp; }
  UInt_t GetTimeStamp() const           { return fTimeStamp;    }

  void Reset();
private:
  UInt_t fTimeStamp;               // time stamp of the event

  ClassDef(myEventHeader,1);       // event header information
};
#endif

