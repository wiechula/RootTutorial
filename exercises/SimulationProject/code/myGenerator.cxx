
#include <TFile.h>
#include <TTree.h>
#include <TF1.h>
#include <TMath.h>
#include <TRandom.h>
#include <TDatabasePDG.h>
#include <TParticlePDG.h>
#include <TLorentzVector.h>

#include "myParticle.h"
#include "myEvent.h"
#include "myGenerator.h"


/*
// Class to emulate high energy collisions by creating events with
// particles following parametrised distributions from measured data

// The parametrisation file is create by running in the 'parametrisations'
// directory:

root -l -b -q fit_and_store.C

// Run the simulation first compile the code by running
make
// from the directory with the 'Makefile'

// Then start ROOT (from the directory with the Makefile) with
root -l macros/loadlibs.C

// and run the simulation:
myGenerator generator;
generator.SetParametrisationFileName("parametrisations/parametrisation.root")
generator.Simulate(10000)


for an example of an invariant mass analysis see the macro: macros/InvariantMassAnalysis.C
*/

myGenerator::myGenerator()
: TObject()
, fNumberOfEvents(1)
, fDistributions("parametrisation.root")
, fOutputFile("simulation.root")
{
  // default constructor
  gRandom->SetSeed(0);
}

//_________________________________________________________________________
void myGenerator::SetdEdxSignal(myParticle &part)
{
  //
  // add energy loss information to the particle
  //

  // the energy loss is calculate from the particle velocity (beta*gamma)
  // which is equal to the total momentum diveded by mass
  Double_t expectedEnergyLoss = BetheBlochAleph(part.GetP()/part.GetMass());

  // smear the energy loss with the detector resolution, which is taken
  // to be 6%
  Double_t smearedEnergyLoss  = gRandom->Gaus(expectedEnergyLoss, expectedEnergyLoss * 0.06);

  // set the energy loss to the particle
  part.SetdEdxSignal(smearedEnergyLoss);
}

//_________________________________________________________________________
Double_t myGenerator::BetheBlochAleph(Double_t bg)
{
  //
  // calculate the expected energy loss from beta * gamma
  // this uses a parametrisation which was developed by the
  // ALEPH experiment at CERN
  //
  Double_t kp1=0.76176e-1;
  Double_t kp2=10.632;
  Double_t kp3=0.13279e-4;
  Double_t kp4=1.8631;
  Double_t kp5=1.9479;
  
  Double_t beta = bg/TMath::Sqrt(1.+ bg*bg);
  
  Double_t aa = TMath::Power(beta,kp4);
  Double_t bb = TMath::Power(1./bg,kp5);
  
  bb=TMath::Log(kp3+bb);
  
  return (kp2-aa-bb)*kp1/aa;
}

//_________________________________________________________________________
myGenerator::~myGenerator()
{
  // destructor
}

//_________________________________________________________________________
void myGenerator::Simulate(Int_t nevents)
{
  //
  // run the simulation
  // if nevents>0 then nevents will be used, otherwise the
  // global variable fNumberOfEvents
  //

  if (nevents<1) nevents=fNumberOfEvents;

  //==============================================================
  // ====| read the input distributions |=========================
  TFile distFile(fDistributions);
  TF1 *fpt_pion     = (TF1*)distFile.Get("fpt_pion");
  TF1 *fpt_kaon     = (TF1*)distFile.Get("fpt_kaon");
  TF1 *fpt_proton   = (TF1*)distFile.Get("fpt_proton");
  TF1 *multiplicity = (TF1*)distFile.Get("multiplicity");
  distFile.Close();

  //==============================================================
  // ===| calculate the relative particle abundances |============
  Double_t pionFraction   = fpt_pion  ->Integral(fpt_pion->GetXmin(),   fpt_pion->GetXmax());
  Double_t kaonFraction   = fpt_kaon  ->Integral(fpt_kaon->GetXmin(),   fpt_kaon->GetXmax());
  Double_t protonFraction = fpt_proton->Integral(fpt_proton->GetXmin(), fpt_proton->GetXmax());
  
  Double_t particleSum=pionFraction+kaonFraction+protonFraction;

  pionFraction   /= particleSum;
  kaonFraction   /= particleSum;
  protonFraction /= particleSum;
  
  //==============================================================
  // ===| create particle and event objects and the output tree |=
  myParticle particle;
  myEvent    *event = new myEvent;
  
  TFile outputFile(fOutputFile,"recreate");
  TTree eventTree("events","My generated events");
  eventTree.Branch("event",&event);
  
  //==============================================================
  // ===| create the events |=====================================
  for (UInt_t ievent=0; ievent<nevents; ++ievent){
    event->SetVz(gRandom->Gaus(0,7));

    //========================================================================
    // get the number of particles for this event
    // from the multiplicity probability distribution
    // this needs to be an integer number, therefore round to
    //   nearest integer value
    Int_t numberOfParticles = TMath::Nint(multiplicity->GetRandom());

    //========================================================================
    // ===| add default particles to the event (pions, kaons, protons)|=======
    for (Int_t iparticle=0; iparticle<numberOfParticles; ++iparticle) {
      // random number to decide on the particle specie
      Double_t specie          = gRandom->Rndm();

      // particle properties
      // we chose a flat distribution in the azimuthal angle (phi)
      //   due to symmatry considerations
      // we take the distribution flat in pseudorapidity which is
      //   approximately what is measred in reality
      Int_t particleType    = 0;
      Double_t mass         = 0;
      Double_t phi          = gRandom->Uniform(0.0, 2*TMath::Pi());
      Double_t eta          = gRandom->Uniform(-1, 1);
      Double_t pt           = 0;

      // decide on particle specie and use the respective measred pt distribution
      //   to randomly generate the pt of the particle
      if (specie < protonFraction) {
        //proton
        particleType=3;
        mass=0.9383;
        pt=fpt_proton->GetRandom();
      } else if ( specie>=(protonFraction) && specie<(protonFraction+kaonFraction) ) {
        //kaon
        particleType=2;
        mass=0.4937;
        pt=fpt_kaon->GetRandom();
      } else if ( specie>=(protonFraction+kaonFraction) ) {
        //pion
        particleType=1;
        mass=0.1396;
        pt=fpt_pion->GetRandom();
      }

      // smear the pt with the detector resolution
      SmearPt(pt);

      // set the particle properties
      particle.SetPtEtaPhiM(pt,eta,phi,mass);
      particle.SetParticleType(particleType);
      // set the charge of the particle 50% negative and 50% postive
      if (gRandom->Rndm()<0.5) particle.SetCharge(1);
      else                     particle.SetCharge(-1);

      // set the simulated energy loss expected in a TPC
      SetdEdxSignal(particle);
      
      // add the particle to the event
      event->AddParticle(particle);
    }

    //==============================================================
    // ===| Add some decay particles |==============================
    AddDecayParticles(*event, fpt_proton);

    // fill the event to the tree and reset the event information
    eventTree.Fill();
    event->Reset();
  }

  // write the tree and close the file
  outputFile.Write();
  outputFile.Close();

  
  //==============================================================
  // ===| cleanup |===============================================
  // delete remaining objects that were created in this function
  delete fpt_pion;
  delete fpt_kaon;
  delete fpt_proton;
  delete multiplicity;
}

//_________________________________________________________________________
void myGenerator::SmearPt(Double_t &pt)
{
  //
  // smear the pt
  // use a simple linear dependence of the relative pt
  // resolution on pt: 1% at 0GeV up to 10% at 50GeV
  //

  const Double_t ptResolution=0.01+0.09/50.*pt;

  // The ptResolution above is a relative resolution
  // for the absolute resolution we need to multiply
  // by the value itself
  pt=gRandom->Gaus(pt,ptResolution*pt);
}

//_________________________________________________________________________
void myGenerator::AddDecayParticles(myEvent &ev, TF1* fpt)
{
  //
  // Add decay particles
  // in 10% of the cases add a phi, a lambda and an anti-lambda
  // in  1% of the cases add a jpsi
  // for simplicity use the pt distribution of the proton
  //

  // random number to decide on the specie
  Double_t specie          = gRandom->Rndm();

  // particle properties
  Int_t particleType    = 0;
  Double_t mass         = 0;
  Double_t phi          = gRandom->Uniform(0.0, 2*TMath::Pi());
  Double_t eta          = gRandom->Uniform(-1, 1);
  Double_t pt           = fpt->GetRandom();

  // lorentz vectors for mother and daughters
  TLorentzVector vMother;
  TLorentzVector vDaughter1;
  TLorentzVector vDaughter2;

  // particle information added to the event
  // for mother and daughters
  myParticle partMother;
  myParticle partD1;
  myParticle partD2;

  // by default set the first daughter to positive
  // and the second daughter to negative charge
  partD1.SetCharge( 1);
  partD2.SetCharge(-1);

  // use the PDG data base store in ROOT to retrieve the particle mass
  // you can get a full list of particlies in the
  // root pdg data base with
  // TDatabasePDG::Instance()->ParticleList()->Print(); >/tmp/part
  TDatabasePDG *pDataBase = TDatabasePDG::Instance();
  TParticlePDG *motherPDG=0x0;
  
  if (specie<0.1) {
    // phi
    particleType=4;
    motherPDG=pDataBase->GetParticle("phi");
    partD1.SetParticleType(2);
    partD2.SetParticleType(2);
  } else if (specie<0.2) {
    // lambda
    particleType=6;
    motherPDG=pDataBase->GetParticle("Lambda0");
    partD1.SetParticleType(3);
    partD2.SetParticleType(1);
  } else if (specie<0.3) {
    // anti lambda
    particleType=7;
    motherPDG=pDataBase->GetParticle("Lambda0_bar");
    // in case of the anti lambda the first particle
    // is the anti proton which has negative charge
    partD1.SetCharge(-1);
    partD2.SetCharge(1);
    partD1.SetParticleType(3);
    partD2.SetParticleType(1);
  } else if (specie<0.31) {
    // jpsi
    particleType=5;
    motherPDG=pDataBase->GetParticle("J/psi");
    partD1.SetParticleType(0);
    partD2.SetParticleType(0);
  } else {
    // no particle will be added
    return;
  }

  mass=motherPDG->Mass();

  // setup the mother particle's lorentz vector and let it
  // decay into its daughters
  vMother.SetPtEtaPhiM(pt,eta,phi,mass);
  DecayMother(vMother, vDaughter1, vDaughter2,particleType);

  // setup particles and smear the pt
  // since the mother is not detectable we don't smear the pt
  // add all particles to the event

  // mother
  pt=vMother.Pt();
  partMother.SetPtEtaPhiM(pt, vMother.Eta(), vMother.Phi(), vMother.M());
  partMother.SetDetectable(kFALSE);
  partMother.SetParticleType(particleType);
  Int_t npart=ev.GetNumberOfParticles();
  partMother.SetDaughterLimits(npart+1, npart+2);
  ev.AddParticle(partMother);

  // daughter 1
  pt=vDaughter1.Pt();
  SmearPt(pt);
  partD1.SetPtEtaPhiM(pt, vDaughter1.Eta(), vDaughter1.Phi(), vDaughter1.M());
  SetdEdxSignal(partD1);
  partD1.SetMother(npart);
  ev.AddParticle(partD1);
  
  // daughter 2
  pt=vDaughter2.Pt();
  SmearPt(pt);
  partD2.SetPtEtaPhiM(pt, vDaughter2.Eta(), vDaughter2.Phi(), vDaughter2.M());
  SetdEdxSignal(partD2);
  partD2.SetMother(npart);
  ev.AddParticle(partD2);
  
}

//_________________________________________________________________________
void myGenerator::DecayMother(const TLorentzVector &vMother,
                              TLorentzVector &vDaughter1,
                              TLorentzVector &vDaughter2,
                              Int_t decayType)
{
  //
  // decay a particle
  // decayType:  4 - phi    -> K+K-
  //             5 - J/psi  -> e+e-
  //             6 - Lambda -> p pi-
  //             7 - AntiLambda -> anti p pi+
  //

  if (decayType<4||decayType>7) return;
  // get the daughter particles from the PDG data base
  // this is only used for the mass
  TDatabasePDG *pDataBase = TDatabasePDG::Instance();
  TParticlePDG *d1 = 0, *d2 = 0;
  if (decayType==4) {
    d1 = pDataBase->GetParticle("K-");
    d2 = pDataBase->GetParticle("K+");
  } else if (decayType==5) {
    d1 = pDataBase->GetParticle("e-");
    d2 = pDataBase->GetParticle("e+");
  } else if (decayType==6) {
    d1 = pDataBase->GetParticle("proton");
    d2 = pDataBase->GetParticle("pi-");
  } else if (decayType==7) {
    d1 = pDataBase->GetParticle("antiproton");
    d2 = pDataBase->GetParticle("pi+");
  }
  
  const Double_t m1=d1->Mass();
  const Double_t m2=d2->Mass();
  const Double_t mm=vMother.M();
  
  // the total energy of the decay particles particles in the
  // restframe of the mother
  Double_t e1 = (mm*mm - m2*m2 + m1*m1)/(2*mm);
  Double_t e2 = (mm*mm + m2*m2 - m1*m1)/(2*mm);
  
  // the total momentum of the decay
  Double_t p1 = TMath::Sqrt( (mm*mm - (m1+m2)*(m1+m2)) * (mm*mm - (m1-m2)*(m1-m2))) / (2*mm);
  
  // calclate the momentum components in the rest frame of the mother
  // in the rest frame the decay is rotational symmetric
  // this means it needs to be equally distributed over solid angle:
  // dOmega = sin(theta) dtheta dphi = dcostheta dphi
  // thus being equivalent to be flat in costheta and phi
  Double_t costheta = gRandom->Uniform(-1.,1.);
  Double_t sintheta = TMath::Sqrt((1. + costheta)*(1. - costheta));
  Double_t phi      = gRandom->Uniform(0., 2.*TMath::Pi());

  // do the coordinate transformation from sperical to cartesian coordinates
  Double_t px1 = p1 * sintheta * TMath::Cos(phi);
  Double_t py1 = p1 * sintheta * TMath::Sin(phi);
  Double_t pz1 = p1 * costheta;
  
  // 4-momenta of the daughter particles
  vDaughter1.SetPxPyPzE(-px1,-py1,-pz1,e1);   //in the mother's rest frame
  vDaughter2.SetPxPyPzE( px1, py1, pz1,e2);
  
  // calclate the required lorentz boost given by the mother particle
  TVector3 betacm;
  betacm = (vMother.Vect()*(1./vMother.E()));
  
  // boost daughters to the lab frame
  vDaughter1.Boost(betacm);
  vDaughter2.Boost(betacm);
}
