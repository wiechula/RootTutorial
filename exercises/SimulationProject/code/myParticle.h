#ifndef myParticle_H
#define myParticle_H

#include <TObject.h>

class TLorentzVector;

class myParticle : public TObject {
public:
  myParticle();
  myParticle(const myParticle &ev);
  virtual ~myParticle();

  // ===| 4-momentum information |==========================================================
  void SetPx  (Double_t px) { fPx=px;  }
  void SetPy  (Double_t py) { fPy=py;  }
  void SetPz  (Double_t pz) { fPz=pz;  }
  void SetMass(Double_t m ) { fMass=m; }

  void SetPxPyPzM(Double_t px, Double_t py, Double_t pz, Double_t m) {fPx=px; fPy=py; fPz=pz; fMass=m;}
  void SetPtEtaPhiM(Double_t pt, Double_t eta, Double_t phi, Double_t m);
  
  Double_t GetPx()    const { return fPx;   }
  Double_t GetPy()    const { return fPy;   }
  Double_t GetPz()    const { return fPz;   }
  Double_t GetMass()  const { return fMass; }

  Double_t GetP()     const;
  Double_t GetPt()    const;
  Double_t GetPhi()   const;
  Double_t GetEta()   const;
  Double_t GetTheta() const;

  void  SetLorentzVector(TLorentzVector &v, Double_t massAssumption) const;

  // ===| vertex information |==========================================================
  void SetVxVyVz(Double_t vx, Double_t vy, Double_t vz) {fVx=vx; fVy=vy; fVz=vz;}

  Double_t GetVx() const { return fVx; }
  Double_t GetVy() const { return fVy; }
  Double_t GetVz() const { return fVz; }

  // ===| particle type |==========================================================
  // 0: electron; 1: pion; 2: kaon; 3: proton; 4: phi; 5: J/psi; 6: lambda; 7: anti-lambda
  void SetParticleType(Int_t type) {fParticleType=type;    }

  Int_t GetParticleType() const    { return fParticleType; }

  // ===| detector signals |==========================================================
  void SetdEdxSignal(Double_t dEdx) { fdEdxSignal=dEdx;  }

  Double_t GetdEdxSignal() const    { return fdEdxSignal; }

  // ===| other information |==========================================================
  void SetCharge(Char_t c) {fCharge=c;}
  void SetDetectable(Bool_t det) {fIsDetectable=det;}
  void SetDaughterLimits(Int_t first, Int_t last) {fFirstDaughter=first; fLastDaughter=last;}
  void SetMother(Int_t mother) {fMother=mother;}

  Char_t GetCharge()        const { return fCharge;        }
  Bool_t IsDetectable()     const { return fIsDetectable;  }
  Int_t  GetFirstDaughter() const { return fFirstDaughter; }
  Int_t  GetLastDaughter()  const { return fLastDaughter;  }
  Int_t  GetMother()        const { return fMother;        }
  
private:
  Double32_t fPx;                // x momentum of the particle
  Double32_t fPy;                // y momentum of the particle
  Double32_t fPz;                // z momentum of the particle
  Double32_t fMass;              // mass of the particle

  Double32_t fVx;                // production point of the particle in x-direction
  Double32_t fVy;                // production point of the particle in y-direction
  Double32_t fVz;                // production point of the particle in z-direction
  
  Int_t      fParticleType;      // particle type from MC generation
  Char_t     fCharge;            // charge of the particle

  Double32_t fdEdxSignal;        // emulated dEdx signal of a TPC

  Bool_t     fIsDetectable;      // if the particle is detectable

  Int_t      fFirstDaughter;     // the position of the first daughter in the particle list
  Int_t      fLastDaughter;      // the position of the last daughter in the particle list
  Int_t      fMother;            // the position of the mother particle in the particle list
  
  ClassDef(myParticle,1);        // particle information
};


#endif
