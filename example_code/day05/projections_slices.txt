// create a 2D histogram and get a projection
TH2D h2("h2","2D histo",100,-5,5,20,0,20);
h2.FillRandom("gaus",100000);
h2.Draw("colz");
h2.ProjectionX("mypx",10,10);
TH1D *mypx=(TH1D*)gDirectory->Get("mypx");



// fit slices of a 2D histogram
TH2D h2("h2","2D histo",100,-5,5,100,-5,5);
TF2 f2("f2","exp(-(x**2+y**2))",-5,5,-5,5)
h2.FillRandom("f2",100000);
h2.FitSlicesY()
.ls
h2_1->SetMarkerStyle(20); h2_1->SetMarkerSize(1);
h2->Draw("colz")
h2_1->Draw("same")
