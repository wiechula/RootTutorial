// check if a file exists, returns 'kFALSE'
// if the file exists and 'kTRUE' in case is does not
// the logic is inverse ...
Bool_t exists=gSystem->AccessPathName("/tmp/noFile");
if (exists==kTRUE) cout << "File '/tmp/noFile' does not exist!" << endl;

Bool_t exists=gSystem->AccessPathName("gSystem_examples.txt");
if (exists==kFALSE) cout << "File 'gSystem_examples.txt' exist!" << endl;

// my favourite way of using things -- but this only works for
// linux systems -- is to directly execute linux commands and
// read back the stdout output
// e.g. list all files in the '/tmp' directory
TString result=gSystem->GetFromPipe("ls /tmp");
TObjArray *arr=result.Tokenize("\n");
for (Int_t i=0; i<arr->GetEntriesFast(); ++i) cout << arr->At(i)->GetName() << endl;

// or line by line display the contents of this file:
TString result=gSystem->GetFromPipe("cat gSystem_examples.txt");
TObjArray *arr=result.Tokenize("\n");
for (Int_t i=0; i<arr->GetEntriesFast(); ++i) cout << arr->At(i)->GetName() << endl;

