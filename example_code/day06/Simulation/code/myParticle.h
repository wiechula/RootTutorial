#ifndef myParticle_H
#define myParticle_H

#include <TObject.h>

class myParticle : public TObject {
public:
  myParticle();
  myParticle(const myParticle &ev);
  virtual ~myParticle();

  void SetPx(Double_t px) { fPx=px; }

  Double_t GetPx() const { return fPx; }
private:
  Double_t fPx;                // x momentum of the particle
  
  ClassDef(myParticle,1);      // particle information
};


#endif
